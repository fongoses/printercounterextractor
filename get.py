# -*- coding: UTF-8 -*-
from pysnmp.entity.rfc3413.oneliner import cmdgen
from config import *

def get_snmp(host,oid):
    e,es,ei,vb = cmdgen.CommandGenerator().getCmd(cmdgen.CommunityData('none','public',0),cmdgen.UdpTransportTarget((host,161)),oid)
    return str(vb[0][1])

def get_snmp_next(host,oid):
    e, es, ei, vb = cmdgen.CommandGenerator().nextCmd(cmdgen.CommunityData('public'),
        cmdgen.UdpTransportTarget((host, 161)), oid)
    return vb


line = ''
header = 'Host,Modelo,Num_Serie,Pg_Preto_Branco,Pg_Colorida\n'
f = open(ARQUIVO,'w')
print header[:-1]
f.write(header)

# host,modelo,número de série,contador P&B,contador Colororido
for i in IMPRESSORAS:
    modelo = i[0]
    host   = i[1]
    serial = get_snmp(host,MODELOS[modelo][SERIAL_OID])
    pb_counter = get_snmp(host,MODELOS[modelo][PB_OID])
    if len(MODELOS[modelo]) > COLOR_OID:
        color_counter = get_snmp(host,MODELOS[modelo][COLOR_OID])
    else:
        color_counter = ''
    line = '%s,%s,%s,%s,%s\n' % (host,modelo,serial,pb_counter,color_counter)
    f.write(line)
    print line[:-1]
f.close()

if len(ENVIAR_PARA) > 0:
    import win32com.client
    gw = win32com.client.Dispatch('NovellGroupWareSession')
    acc = gw.MultiLogin(USER,None,PASSWORD,1)
    msg = acc.MailBox.Messages.Add("GW.Message.Mail", "Draft")
    msg.fromtext = "Mpf"
    for r in ENVIAR_PARA:
        msg.Recipients.Add(r)
    msg.Subject.PlainText = u"Relatório de Contadores MPF"
    msg.BodyText.PlainText = u"Segue anexo relatório dos contadores da PRM-Lajeado/RS."
    msg.Attachments.Add(ARQUIVO,1)
    msg.send()
    gw.quit()
