# -*- coding: UTF-8 -*-
SERIAL_OID = 0
PB_OID     = 1
COLOR_OID  = 2

MODELOS = {
    'MC780':[
    "1.3.6.1.2.1.43.5.1.1.17.1", # OID PARA NÚMERO DE SÉRIE
    "1.3.6.1.4.1.2001.1.1.1.1.11.1.10.137.0", # OID para NÚMERO DE PÁGINAS P&B
    "1.3.6.1.4.1.2001.1.1.1.1.11.1.10.140.0", # OID PARA PAGS COLORIDAS
    ],
    'M4020ND':[
    "1.3.6.1.2.1.43.5.1.1.17.1", # OID PARA NÚMERO DE SÉRIE
    "1.3.6.1.2.1.43.10.2.1.4.1.1", # OID para NÚMERO DE PÁGINAS P&B
    ]

}

IMPRESSORAS = [
    # ('MC780','1.1.1.1'),
    # ('M4020ND','2.2.2.2'),

]

ARQUIVO = 'contadores.csv'

ENVIAR_PARA = [

]
USER = ''
PASSWORD = ''
