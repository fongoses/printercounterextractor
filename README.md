# PrinterCounterExtractor
- - -

  O objetivo deste script é recuperar os contadores de impressão das impressoras laser e enviar o arquivo gerado via e-mail.

# Requisitos

   Os seguintes softwares e bibliotecas precisam estar instaladas para este script funcionar:  

  * Windows 7
  * [Python 2.7](https://www.python.org/ftp/python/2.7.9/python-2.7.9.msi)
  * [Pysnmp](http://pysnmp.sourceforge.net/download.html)
  * [Python for Windows Extensions](http://downloads.sourceforge.net/project/pywin32/pywin32/Build%20219/pywin32-219.win32-py2.7.exe?r=http%3A%2F%2Fsourceforge.net%2Fprojects%2Fpywin32%2Ffiles%2Fpywin32%2FBuild%2520219%2F&ts=1434044361&use_mirror=iweb)
  * Cliente de e-mail [Groupwise](https://www.novell.com/products/groupwise/)


# Instalação

  1. Baixe o [arquivo](https://bitbucket.org/rosaldanha/printercounterextractor/get/master.zip)
  2. Descompacte em qualquer pasta
  3. Edite o arquivo config.py e altere os seguintes parametros:

### IMPRESSORAS

  >IMPRESSORAS = [
  >
  >    ('MC780','1.1.1.1'),
  >
  > ]

  Para cada impressora inclua uma linha entre os [ ], no seguinte formato:

  > ('modelo_da_impressora','nome do host ou ip'),

### ENVIAR_PARA

  >ENVIAR_PARA = [
  >
  >    'fornecedor@fornecedor.com',
  >
  >    'meu_email@mpf.mp.br',
  >
  >]

  Inclua nesta lista os e-mails que irão receber o relatório. Um por linha entre os [ ], obedecendo o seguinte formato:

  >    'email@email.com',

  Obs: Se esta lista estiver em branco, nenhum e-mail será enviado, apenas o arquivo .csv será gerado, neste caso não é necessário instalar a biblioteca Python for Windows Extensions.

### USER PASSWORD

  Informe o seu login e senha caso deseje enviar os e-mails usando o groupwise.
  Use o seguinte formato:

  >USER = 'MeuLogin'
  >
  >PASSWORD = 'MinhaSenha'  

# Executando

  Para executar digite na linha de comando, dentro da pasta criada:

  > python get.py

# Dúvidas, sugestões, críticas

  [Abra um chamado](https://bitbucket.org/rosaldanha/printercounterextractor/issues/new)
